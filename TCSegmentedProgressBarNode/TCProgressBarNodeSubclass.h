//
//  TCProgressBarNodeSubclass.h
//  TCSegmentedProgressBarNode
//
//  Created by Charles Chamblee on 8/12/15.
//  Copyright (c) 2015 Tony Chamblee. All rights reserved.
//

#import "TCProgressBarNode.h"

@interface TCProgressBarNode (ForSubclassEyesOnly)

@property (nonatomic, strong) SKCropNode *fillCropNode;

- (SKTexture *)textureFromLayer:(CALayer *)layer;

@end
