//
//  AppDelegate.h
//  TCSegmentedProgressBarNode
//
//  Created by Charles Chamblee on 8/12/15.
//  Copyright (c) 2015 Tony Chamblee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

