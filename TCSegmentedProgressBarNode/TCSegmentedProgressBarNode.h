//
//  TCSegmentedProgressBarNode.h
//  TCSegmentedProgressBarNode
//
//  Created by Charles Chamblee on 8/12/15.
//  Copyright (c) 2015 Tony Chamblee. All rights reserved.
//

#import "TCProgressBarNode.h"

@interface TCSegmentedProgressBarNode : TCProgressBarNode

@property (nonatomic) NSUInteger filledSegments;

- (instancetype)initWithSize:(CGSize)size
             backgroundColor:(UIColor *)backgroundColor
                   fillColor:(UIColor *)fillColor
                 borderColor:(UIColor *)borderColor
                 borderWidth:(CGFloat)borderWidth
                cornerRadius:(CGFloat)cornerRadius
            numberOfSegments:(NSUInteger)numberOfSegments
                segmentColor:(UIColor *)segmentColor;

- (instancetype)initWithBackgroundTexture:(SKTexture *)backgroundTexture
                              fillTexture:(SKTexture *)fillTexture
                           overlayTexture:(SKTexture *)overlayTexture
                         numberOfSegments:(NSUInteger)numberOfSegments
                             segmentColor:(UIColor *)segmentColor;

@property (nonatomic, readonly) NSUInteger numberOfSegments;

@end
