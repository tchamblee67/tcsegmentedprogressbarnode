//
//  AppDelegate.m
//  TCSegmentedProgressBarNode
//
//  Created by Charles Chamblee on 8/12/15.
//  Copyright (c) 2015 Tony Chamblee. All rights reserved.
//

#import "AppDelegate.h"
#import "TCViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.window.rootViewController = [[TCViewController alloc] init];
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
