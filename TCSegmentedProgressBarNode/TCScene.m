//
//  TCScene.m
//  TCProgressBarNode
//
//  Created by Charles Chamblee on 7/23/15.
//  Copyright (c) 2015 Tony Chamblee. All rights reserved.
//

#import "TCScene.h"
#import "TCSegmentedProgressBarNode.h"

const NSTimeInterval kSegmentIncrementInterval = 0.5;

@interface TCScene ()

@property (nonatomic, strong) TCSegmentedProgressBarNode *progressBarNode;

@property (nonatomic) NSTimeInterval startTime;

@end

@implementation TCScene

#pragma mark - Init / Dealloc

- (id)initWithSize:(CGSize)size
{
    self = [super initWithSize:size];
    
    if (self)
    {
        self.backgroundColor = [UIColor whiteColor];
    }
    
    return self;
}

#pragma mark - Scene Lifecycle

- (void)didMoveToView:(SKView *)view
{
    [super didMoveToView:view];
    
    self.size = view.bounds.size;
    
    [self configureProgressBarNode];
}

#pragma mark - Configuration

- (void)configureProgressBarNode
{
    self.progressBarNode = [[TCSegmentedProgressBarNode alloc] initWithSize:CGSizeMake(300.0, 32.0)
                                                            backgroundColor:[UIColor darkGrayColor]
                                                                  fillColor:[UIColor yellowColor]
                                                                borderColor:[UIColor lightGrayColor]
                                                                borderWidth:2.0
                                                               cornerRadius:4.0
                                                           numberOfSegments:8
                                                               segmentColor:[UIColor lightGrayColor]];
    
    self.progressBarNode.position = CGPointMake(round(self.size.width / 2.0), round(self.size.height / 2.0f));
    self.progressBarNode.filledSegments = self.progressBarNode.numberOfSegments;
    
    [self.scene addChild:self.progressBarNode];
}

- (void)update:(NSTimeInterval)currentTime
{
    [super update:currentTime];
    
    if (self.startTime == 0)
    {
        self.startTime = currentTime;
        return;
    }
    
    NSTimeInterval elapsedTime = currentTime - self.startTime;
    
    if (elapsedTime >= kSegmentIncrementInterval)
    {
        self.startTime = currentTime;
        
        NSInteger filledSegments = self.progressBarNode.filledSegments - 1;
        
        if (filledSegments == -1)
        {
            filledSegments = self.progressBarNode.numberOfSegments;
        }
        
        self.progressBarNode.filledSegments = filledSegments;
    }
}

@end
