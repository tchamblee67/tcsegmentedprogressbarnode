//
//  TCSegmentedProgressBarNode.m
//  TCSegmentedProgressBarNode
//
//  Created by Charles Chamblee on 8/12/15.
//  Copyright (c) 2015 Tony Chamblee. All rights reserved.
//

#import "TCSegmentedProgressBarNode.h"
#import "TCProgressBarNodeSubclass.h"

@interface TCSegmentedProgressBarNode ()

@property (nonatomic, strong) SKSpriteNode *segmentOverlaySpriteNode;
@property (nonatomic, strong) UIColor *segmentColor;

@property (nonatomic) NSUInteger numberOfSegments;

@end

@implementation TCSegmentedProgressBarNode

#pragma mark - Properties

- (void)setFilledSegments:(NSUInteger)filledSegments
{
    if (_filledSegments != filledSegments)
    {
        _filledSegments = filledSegments;
        
        [self filledSegmentsDidChange];
    }
}

#pragma mark - Init / Dealloc

- (instancetype)init
{
    return [self initWithSize:CGSizeZero
              backgroundColor:nil
                    fillColor:nil
                  borderColor:nil
                  borderWidth:0.0
                 cornerRadius:0.0
            numberOfSegments:0
                 segmentColor:nil];
}

- (instancetype)initWithSize:(CGSize)size
             backgroundColor:(UIColor *)backgroundColor
                   fillColor:(UIColor *)fillColor
                 borderColor:(UIColor *)borderColor
                 borderWidth:(CGFloat)borderWidth
                cornerRadius:(CGFloat)cornerRadius
            numberOfSegments:(NSUInteger)numberOfSegments
                segmentColor:(UIColor *)segmentColor
{
    NSParameterAssert(segmentColor);
    
    self = [super initWithSize:size
               backgroundColor:backgroundColor
                     fillColor:fillColor
                   borderColor:borderColor
                   borderWidth:borderWidth
                  cornerRadius:cornerRadius];
    
    if (self)
    {
        _numberOfSegments = numberOfSegments;
        _segmentColor = segmentColor;
        
        [self segmentedProgressBarNodeCommonInit];
    }
    
    return self;
}

- (instancetype)initWithBackgroundTexture:(SKTexture *)backgroundTexture
                              fillTexture:(SKTexture *)fillTexture
                           overlayTexture:(SKTexture *)overlayTexture
                         numberOfSegments:(NSUInteger)numberOfSegments
                             segmentColor:(UIColor *)segmentColor
{
    NSParameterAssert(segmentColor);

    self = [super initWithBackgroundTexture:backgroundTexture
                                fillTexture:fillTexture
                             overlayTexture:overlayTexture];
    
    if (self)
    {
        _numberOfSegments = numberOfSegments;
        _segmentColor = segmentColor;
        
        [self segmentedProgressBarNodeCommonInit];
    }
    
    return self;
}

#pragma mark - Initialization

- (void)segmentedProgressBarNodeCommonInit
{
    [self initializeSegmentOverlaySpriteNode];
}

- (void)initializeSegmentOverlaySpriteNode
{
    CAShapeLayer *shapeLayer = [CAShapeLayer new];
    UIBezierPath *bezierPath = [UIBezierPath new];
    
    shapeLayer.frame = CGRectMake(0.0, 0.0, self.size.width, self.size.height);
    
    shapeLayer.lineWidth = 1.0f;
    shapeLayer.strokeColor = self.segmentColor.CGColor;
    
    CGFloat halfBorderWidth = round(self.borderWidth / 2.0);
    CGFloat totalFillWidth = self.size.width - self.borderWidth;
    CGFloat totalFillHeight = self.size.height - self.borderWidth;
    CGFloat step = totalFillWidth / self.numberOfSegments;
    CGFloat x = halfBorderWidth + step;
    CGFloat y = halfBorderWidth;
    
    for (NSInteger index = 0; index < self.numberOfSegments - 1; index++)
    {
        [bezierPath moveToPoint:CGPointMake(x, y)];
        [bezierPath addLineToPoint:CGPointMake(x, y + totalFillHeight)];
        
        x += step;
    }
    
    shapeLayer.path = bezierPath.CGPath;
    
    SKTexture *texture = [self textureFromLayer:shapeLayer];
    self.segmentOverlaySpriteNode = [SKSpriteNode spriteNodeWithTexture:texture];
    
    [self.fillCropNode addChild:self.segmentOverlaySpriteNode];
}

#pragma mark - Interface Updates

- (void)filledSegmentsDidChange
{
    CGFloat progress = (CGFloat)self.filledSegments / (CGFloat)self.numberOfSegments;
    self.progress = progress;
}

@end
