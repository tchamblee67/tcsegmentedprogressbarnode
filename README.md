A segmented flavor of [TCProgressBarNode](https://bitbucket.org/tchamblee67/tcprogressbarnode), a progress bar node for SpriteKit.

![SegmentedProgressBar.gif](https://bitbucket.org/repo/6r7Eab/images/372046913-SegmentedProgressBar.gif)

http://tonychamblee.com/2015/08/12/tcsegmentedprogressbarnode/